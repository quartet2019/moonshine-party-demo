#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

CPPFLAGS += -ftrack-macro-expansion=0 -fno-diagnostics-show-caret
