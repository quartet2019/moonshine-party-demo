// TGA parser
// (C)2019 Duddie of Quartet Inc.


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>


#include "tga.h"
#include "logger.h"


#if 0

void parse_tga(uint8_t *b, uint32_t size) {

    tga_header_t *t = (tga_header_t *)b;

    printf("idlen 0x%x\n", t->idlength);
    
    printf("CMAP Origin: %d Len: %d Size: %d\n", t->colourmaporigin, t->colourmaplength, t->colourmapdepth);
    printf("X: %d Y: %d W: %d H: %d\n", t->x_origin, t->y_origin, t->width, t->height);

    uint32_t p_off = sizeof(*t) + t->idlength + (t->colourmaplength - t->colourmaporigin) * (t->colourmapdepth / 8);
    printf("Pix Idx 0x%x\n", p_off);
}

#endif
uint8_t * tga_get_pixel_data(uint8_t *b) {
    tga_header_t *t = (tga_header_t *)b;
    uint32_t p_off = sizeof(*t) + t->idlength + (t->colourmaplength - t->colourmaporigin) * (t->colourmapdepth / 8);
    return p_off;
}


