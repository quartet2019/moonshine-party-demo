#ifndef _BOARD_CONTROL_H
#define _BOARD_CONTROL_H

void board_init(void);
int32_t board_get_button(void);

#define BX_BUTTON  GPIO_NUM_0
#define BX_LED     GPIO_NUM_5

#endif // _BOARD_CONTROL_H