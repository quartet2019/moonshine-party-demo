#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#define NO_DITHERING 1

#include "FastLED.h"

extern "C" {
#include "events.h"
#include "arch_generic.h"
#include "wifi.h"
#include "logger.h"
#include "app_config.h"
#include "board_control.h"

#include "qrt_demo.h"
#include "multicast.h"
};




CRGBPalette16 currentPalette;
TBlendType    currentBlending;

extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 IRAM_ATTR myRedWhiteBluePalette_p;

#include "palettes.h"

#define NUM_LEDS QRT_BUF_SIZE
#define DATA_PIN 4 
#define BRIGHTNESS  255
#define LED_TYPE    WS2812
#define COLOR_ORDER RGB
CRGB leds[NUM_LEDS];

static const char *TAG = "main";

extern "C" {
  void app_main();
}

void ChangePalettePeriodically(){

  uint8_t secondHand = (millis() / 1000) % 60;
  static uint8_t lastSecond = 99;

  if( lastSecond != secondHand) {
    lastSecond = secondHand;
    if( secondHand ==  0)  { currentPalette = RainbowColors_p;         currentBlending = LINEARBLEND; }
    if( secondHand == 10)  { currentPalette = RainbowStripeColors_p;   currentBlending = NOBLEND;  }
    if( secondHand == 15)  { currentPalette = RainbowStripeColors_p;   currentBlending = LINEARBLEND; }
    if( secondHand == 20)  { SetupPurpleAndGreenPalette();             currentBlending = LINEARBLEND; }
    if( secondHand == 25)  { SetupTotallyRandomPalette();              currentBlending = LINEARBLEND; }
    if( secondHand == 30)  { SetupBlackAndWhiteStripedPalette();       currentBlending = NOBLEND; }
    if( secondHand == 35)  { SetupBlackAndWhiteStripedPalette();       currentBlending = LINEARBLEND; }
    if( secondHand == 40)  { currentPalette = CloudColors_p;           currentBlending = LINEARBLEND; }
    if( secondHand == 45)  { currentPalette = PartyColors_p;           currentBlending = LINEARBLEND; }
    if( secondHand == 50)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = NOBLEND;  }
    if( secondHand == 55)  { currentPalette = myRedWhiteBluePalette_p; currentBlending = LINEARBLEND; }
  }

}


void app_main_task(void *pvParameters){
  
  uint32_t i, j, k;

  while(1){
    
    qrt_loop();

#if 0
    //ChangePalettePeriodically();
    static uint8_t startIndex = 0;
    startIndex = startIndex + 1; /* motion speed */
    int k = 0;
    for( int i = 0; i < NUM_LEDS; i++) {
        //leds[i] = ColorFromPalette( currentPalette, startIndex, 64, currentBlending)
        leds[i] = CRGB(buf[i]);//CRGB(buf[i], buf[i], buf[i]);//(CRGB(p[0], p[1], p[2]);
        p += 3;
        startIndex += 3;
    }
#else
    k = 0;
    for(i = 0 ; i < (NUM_LEDS / 8) ; ) {
      for(j = 0 ; j < 8 ; j++) {
        leds[k++] = CRGB(qrt_buf[i * 8 + 7 - j]);
      }
      i++;
      for(j = 0 ; j < 8 ; j++) {
        leds[k++] = CRGB(qrt_buf[i * 8 + j]);
      }
      i++;
    }    
#endif

    FastLED.show();
    delay(20);
  }


}



static int ota_in_progress =  0;

void app_multicast_task(void *pvParameters)
{
  static uint32_t conn_count = 0;
  event_clr(EVENT_GROUP_HASHER, HASHER_EVENT_MINING);

  while (1) {
    event_wait(EVENT_GROUP_WIFI, WIFI_EVENT_GOT_IP);

    if (multicast_loop() < 0) {
      DBG_LOGE(TAG, "Multicast connectivity issue");
      conn_count++;
    }

    if (conn_count > 10) {
      DBG_LOGE(TAG, "Exceeded connection tries");
      conn_count = 0;
      if (ota_in_progress == 0) {
       wifi_reconnect();
      }
    }

    sleep_ms(1000);
  }

}


volatile static int32_t _sc_wait_time = -1;

volatile int watchdog = 0;
volatile bool watchdog_enabled = false;
volatile int8_t watchdog_fails = 0;


static void app_function_task(void *pvParameters)
{
  static int32_t ota_timer = 0;
  static int32_t ota_kick = 30;
  

  while(1)
  {

    if (board_get_button()) {
      DBG_LOGI(TAG, "Button status %d", board_get_button());
      wifi_sc_start();
    }

    if (event_get(EVENT_GROUP_WIFI, WIFI_EVENT_SC_COMPLETED)) {
      wifi_connect();
    }

    if (event_get(EVENT_GROUP_WIFI, WIFI_EVENT_SC_STARTED)) {
      //DBG_LOGI(TAG, "SmartConfig Started");
      if (_sc_wait_time < 0) {
        _sc_wait_time = WIFI_SMARTCONFIG_WAIT_TIME;
      } else if (_sc_wait_time == 0) {
        DBG_LOGI(TAG, "Trying to abort SmartConfig");
        event_clr(EVENT_GROUP_WIFI, WIFI_EVENT_SC_STARTED);
        event_set(EVENT_GROUP_WIFI, WIFI_EVENT_SC_ABORT);
        _sc_wait_time = -1;
      } else {
        _sc_wait_time--;
      }
    }

    if (event_get(EVENT_GROUP_SERVICE, SERVICE_EVENT_RESET_WIFI)) {
      wifi_reset_ssid();
      arch_restart();
    }

    if (event_get(EVENT_GROUP_SERVICE, SERVICE_EVENT_OTA_READY)) {
      ota_timer = ota_kick;
      event_clr(EVENT_GROUP_SERVICE, SERVICE_EVENT_OTA_READY);
    }

    if (ota_timer++ >= ota_kick) {
      if (event_get(EVENT_GROUP_WIFI, WIFI_EVENT_GOT_IP)) {
        //app_obtain_time();
        //app_ota_check();
        if (ota_kick < 1200) ota_kick += 60;
      }
      ota_timer = 0;
    }

    if (watchdog_enabled) {
      watchdog++;
    }
    //ESP_LOGI(TAG, "GPIO: %d", new_stat);
    sleep_ms(2000);
  }
}


void app_main() {
  event_init();
  board_init();
  arch_nvs_init();
  wifi_init();

  FastLED.addLeds<LED_TYPE, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setMaxPowerInVoltsAndMilliamps(5,3000);
  
  xTaskCreate(&app_multicast_task, "app_multicast_task", 8192, NULL, 5, NULL);
  xTaskCreate(&app_function_task, "app_function_task", 8192, NULL, 5, NULL);
  //xTaskCreatePinnedToCore(&blinkLeds, "blinkLeds", 4000, NULL, 5, NULL, 0);
  xTaskCreate(&app_main_task, "app_main_task", 4000, NULL, 5, NULL);
}
