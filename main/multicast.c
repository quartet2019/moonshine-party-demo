#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>
#include "logger.h"
#include "multicast.h"
#include "qrt_demo.h"

#define EXAMPLE_PORT 6000
#define EXAMPLE_GROUP "239.0.0.1"

static const char *TAG = "multicast";

int32_t multicast_loop(void) {
   struct sockaddr_in addr;
   uint32_t addrlen;
   int sock, cnt;
   struct ip_mreq mreq;
   char message[256];

   /* set up socket */
   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) {
     perror("socket");
     exit(1);
   }
   bzero((char *)&addr, sizeof(addr));
   addr.sin_family = AF_INET;
   addr.sin_addr.s_addr = htonl(INADDR_ANY);
   addr.sin_port = htons(EXAMPLE_PORT);
   addrlen = sizeof(addr);

      /* receive */
  if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {        
    DBG_LOGE(TAG, "Bind Failed");
    return -1;
  }    

  mreq.imr_multiaddr.s_addr = inet_addr(EXAMPLE_GROUP);         
  mreq.imr_interface.s_addr = htonl(INADDR_ANY);         

  if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
    DBG_LOGE(TAG, "setsockopt mreq");
    return -1;
  }         
  while (1) {
    memset(message, 0, 256);
    cnt = recvfrom(sock, message, sizeof(message), 0, (struct sockaddr *) &addr, &addrlen);
    if(message[0] == '#' && message[1] == '#' && message[2] == '0') {
      qrt_set_priority_msg(message + 3);
    } else {
      qrt_inject_text(message);
    }
    if (cnt < 0) {
        DBG_LOGE(TAG, "recvfrom failed");
        return -1;
    } else if (cnt == 0) {
        DBG_LOGW(TAG, "Connection lost");
        break;
    }
    printf("%s: message = \"%s\"\n", inet_ntoa(addr.sin_addr), message);
  }
  return 0;
}
