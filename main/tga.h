#ifndef _TGA_H
#define _TGA_H

#pragma pack(push, 1)
typedef struct {
   char  idlength;
   char  colourmaptype;
   char  datatypecode;
   short int colourmaporigin;
   short int colourmaplength;
   char  colourmapdepth;
   short int x_origin;
   short int y_origin;
   short width;
   short height;
   char  bitsperpixel;
   char  imagedescriptor;
} tga_header_t;
#pragma pack(pop)


uint8_t * tga_get_pixel_data(uint8_t *b);

#endif // _TGA_H