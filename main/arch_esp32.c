#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_partition.h"
#include "nvs_flash.h"
#include "nvs.h"

#include "uECC.h"
#include "logger.h"


static const char *TAG = "arch_esp32";


uint32_t arch_get_ms(void) {
  struct timeval tv;
  gettimeofday(&tv,NULL);
  uint32_t time_ms = (tv.tv_usec / 1000) + ((tv.tv_sec & 0xffff) * 1000);
  return time_ms;
}

uint32_t arch_geo(int s)
{
  return 0;
}

void arch_info(void)
{
  /* Print chip information */
  esp_chip_info_t chip_info;
  esp_chip_info(&chip_info);
  DBG_LOGI(TAG, "This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
    chip_info.cores,
    (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
    (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

  DBG_LOGI(TAG, "silicon revision %d, ", chip_info.revision);

  DBG_LOGI(TAG, "%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
    (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

}

void arch_restart(void)
{
  for(int countdown = 5; countdown >= 0; countdown--) {
      DBG_LOGI(TAG, "%d... ", countdown);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
  DBG_LOGI(TAG, "Restarting now.\n");
  fflush(stdout);
  esp_restart();
}

void sleep_ms(uint32_t ms)
{
  vTaskDelay(ms / portTICK_RATE_MS);
}


uint32_t arch_random(void)
{
  return esp_random();
}
// RNG Function for uECC on ESP32

int RNG_Function(uint8_t *dest, unsigned size)
{
  int i;

  /* Print 5 random numbers from 0 to 50 */
  for( i = 0 ; i < size ; i++ ) {
     dest[i] = esp_random();
  }
  return 1;
}

void arch_rng_init(void)
{
  uECC_set_rng(RNG_Function);
}

void arch_nvs_init(void)
{
  // Initialize NVS.
  esp_err_t err = nvs_flash_init();
  if(err == ESP_ERR_NVS_NO_FREE_PAGES) {
    // OTA app partition table has a smaller NVS partition size than the non-OTA
    // partition table. This size mismatch may cause NVS initialization to fail.
    // If this happens, we erase NVS partition and initialize NVS again.
    const esp_partition_t* nvs_partition = esp_partition_find_first(
            ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);
    assert(nvs_partition && "partition table must have an NVS partition");
    ESP_ERROR_CHECK(esp_partition_erase_range(nvs_partition, 0, nvs_partition->size));
    err = nvs_flash_init();
  }
  ESP_ERROR_CHECK(err);

}

int arch_nvs_read(const char *name, void *data, size_t size)
{
  esp_err_t err;
  int retval = 0;
  nvs_handle my_handle;
  // Open
  printf("\n");
  printf("Opening Non-Volatile Storage (NVS) handle... ");
  err = nvs_open("storage", NVS_READWRITE, &my_handle);
  if (err != ESP_OK) {
    printf("Error (%d) opening NVS handle!\n", err);
    return 0;
  } else {
    printf("Done\n");

    // Read
    printf("Reading restart counter from NVS ... ");
    size_t blob_size = size;
    err = nvs_get_blob(my_handle, name, data, &blob_size);
    switch (err) {
      case ESP_OK:
        printf("Blob Size %d\n", blob_size);
        printf("Done\n");
        retval = 1;
        break;
      case ESP_ERR_NVS_NOT_FOUND:
        printf("The value is not initialized yet!\n");
        break;
      default :
        printf("Error (%d) reading!\n", err);
        break;
    }
    // Close
    nvs_close(my_handle);
  }
  return retval;
}

int arch_nvs_write(const char *name, void *data, size_t size)
{
  esp_err_t err;
  int retval = 0;
  nvs_handle my_handle;

  printf("\n");
  printf("Opening Non-Volatile Storage (NVS) handle... ");
  err = nvs_open("storage", NVS_READWRITE, &my_handle);
  if (err != ESP_OK) {
    printf("Error (%d) opening NVS handle!\n", err);
    return 0;
  } else {
    printf("Done\n");

    err = nvs_set_blob(my_handle, name, data, size);
    //err = nvs_set_i32(my_handle, "restart_counter", restart_counter);
    switch (err) {
      case ESP_OK:
        printf("Done\n");
        retval = 1;
        break;
      case ESP_ERR_NVS_NOT_FOUND:
        printf("The value is not initialized yet!\n");
        break;
      default :
        printf("Error (%d) reading!\n", err);
        break;
    }
    // Commit written value.
    // After setting any values, nvs_commit() must be called to ensure changes are written
    // to flash storage. Implementations may write to storage at other times,
    // but this is not guaranteed.
    printf("Committing updates in NVS ... ");
    err = nvs_commit(my_handle);
    printf((err != ESP_OK) ? "Failed!\n" : "Done\n");

    // Close
    nvs_close(my_handle);
  }
  return retval;
}
