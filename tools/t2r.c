// TGA to RAW converter
// (C)2019 Duddie of Quartet Inc.


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "tga.h"

bool opt_decode_pal = false;

uint8_t buf[64*1024];


void print_gfx(uint8_t *pixels, uint32_t w, uint32_t h) {
    uint32_t i, j;
    for (i = 0 ; i < h ; i++) {
        printf("\t");
        for (j = 0 ; j < w ; j++) {
            printf("0x%02x, ", pixels[(h - 1 - i) * w + j]);
        }
        printf("\n");
    }
}

void print_pal(uint8_t *p, uint32_t num_ent) {
        int j;
        printf("\t");
        for (j = 0 ; j < num_ent ; j++) {
            printf("0x%02x, ", *p++);
        }
        printf("\n");
}

char *filename;

void parse_tga(uint8_t *b, uint32_t size) {

    tga_header_t *t = (tga_header_t *)b;

    printf("// idlen 0x%x\n", t->idlength);
    
    printf("// CMAP Origin: %d Len: %d Size: %d\n", t->colourmaporigin, t->colourmaplength, t->colourmapdepth);
    printf("// X: %d Y: %d W: %d H: %d\n", t->x_origin, t->y_origin, t->width, t->height);

    uint32_t p_off = sizeof(*t) + t->idlength + (t->colourmaplength - t->colourmaporigin) * (t->colourmapdepth / 8);
    printf("// Pix Idx 0x%x\n", p_off);

    printf("const uint8_t _gfx_%s[] = {\n", filename);
    printf("\t0x%02x, 0x%02x, \n", t->width, t->height);
    print_gfx(buf + p_off, t->width, t->height);
    printf("};\n");

    if (opt_decode_pal) {
        printf("const uint8_t _pal_%s[] = {\n", filename);
        print_pal(buf + t->idlength, t->colourmaplength * 3);
        printf("};\n");
    }
}


int main(int argc, char **argv) {

    FILE *in;
    size_t filesize;

    if (argc < 2) {
        printf("USAGE: t2r <tga file>\n");
        return 0;
    }
    in = fopen(argv[1], "rb");
    if (in == NULL) {
        printf("Cannot open file %s\n", argv[1]);
        return -1;
    }
    filesize = fread(buf, 1, sizeof(buf), in);
    fclose(in);

    (filename = strrchr(argv[1], '/')) ? ++filename : (filename = argv[1]);
    rindex(filename, '.')[0] = 0;
    //printf("Size of file: %zu\n", filesize);

    if (argc > 2) {
        if (strcmp(argv[2], "-p") == 0)
            opt_decode_pal = true;
    }

    parse_tga(buf, filesize);


    return 0;
}