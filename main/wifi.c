#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "esp_smartconfig.h"

#include "app_config.h"

#include "wifi.h"
#include "events.h"
#include "logger.h"
#include "arch_generic.h"

#include <string.h>

#define WITH_SMARTCONFIG (1)

static const char *TAG = "WiFi";

uint32_t _wifi_has_ssid = WIFI_CONNECT_RETRIES;


#if !WITH_SMARTCONFIG
typedef struct {
  const char *ssid;
  const char *password;
} base_station_t;

base_station_t base_stations[] = {
#if 0
  { "iotw-24-4", "ssssdddd" },
  { "iotw-24-5", "ssssdddd" },
  { "iotw-mr-1", "ssssdddd" },
  { "iotw-mr-2", "ssssdddd" },
  { "iotw-mr-3", "ssssdddd" },
  { "iotw-mr-4", "ssssdddd" },
  { "iotw-mr-5", "ssssdddd" },
  { "iotw-mr-6", "ssssdddd" },
  { "KK", "kk720801"},
#endif
#if 0
  { "iotw-24-6", "ssssdddd" },
  { "iotw-24-7", "ssssdddd" },
  { "iotw-lab-1", "ssssdddd" },
  { "iotw-lab-2", "ssssdddd" },
  { "iotw-lab-3", "ssssdddd" },
  { "iotw-lab-4", "ssssdddd" },
  { "iotw-lab-5", "ssssdddd" },
  { "iotw-lab-6", "ssssdddd" },
  { "KK", "kk720801"},
#endif
#if 0
  { "nixplay-2G", "aaaaaaaa" },
#endif
#if 0
  { "nixplay-0-2G", "nixplay123" },
#endif
#if 0
  // { "iotw-24-4", "ssssdddd" },
  // { "iotw-24-5", "ssssdddd" },
#endif 
#if 1
  { "DuzaKoza", "ssssdddd" },
#endif
  // { "iotw-24", "ssssdddd" },
  // { "iotw-24-1", "ssssdddd" },
  // { "iotw-24-2", "ssssdddd" },
  // { "iotw-24-3", "ssssdddd" },
  // { "iotw-24-4", "ssssdddd" },
  // { "iotw-24-5", "ssssdddd" },
  // { "iotw-24-6", "ssssdddd" },
  // { "iotw-24-7", "ssssdddd" },
  // { "iotw-24-8", "ssssdddd" },
  // { "iotw-24-9", "ssssdddd" },
  // { "Dudofon", "ssssdddd" },
  //  { "FredHK", "Fred5960" },
  //  { "V10_Hong", "boky31249088" },
  //  { "Fred's iPhone", "12345678" },
  //  { "On Bon iPhone", "12345678" },
};

#endif

//static char _wifi_sta_ssid[256] = "xx";
//static char _wifi_sta_password[256] = "xx";

static wifi_config_t wifi_config
#if 1
 = {
  .sta = {
    .ssid = "QUARTET",
    .password = "Quartet2019",
  },
};
#endif

uint8_t bs_pos = 0;
uint8_t bs_pos_old = 0;
uint8_t sta_mac[6];

uint8_t bssid_data[4][6];


extern volatile int watchdog;
extern volatile bool watchdog_enabled;


uint8_t wifi_watchdog = 0;

const char * wifi_get_mac_string(uint8_t *mac)
{
  static char mac_string[18];
  sprintf(mac_string, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  return mac_string;
}

uint8_t * wifi_get_mac(void)
{
  return sta_mac;
}

static void wifi_scan_event(system_event_t *event)
{
  uint16_t apCount = 0;
  esp_wifi_scan_get_ap_num(&apCount);
  printf("Number of access points found: %d\n",event->event_info.scan_done.number);
  if (apCount == 0) {
    return;
  }
  wifi_ap_record_t *list = (wifi_ap_record_t *)malloc(sizeof(wifi_ap_record_t) * apCount);
  ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&apCount, list));
  int i;
  printf("======================================================================\n");
  printf("             SSID             |    RSSI    |           AUTH           \n");
  printf("======================================================================\n");
  for (i=0; i<apCount; i++) {
    char *authmode;
    switch(list[i].authmode) {
      case WIFI_AUTH_OPEN:
      authmode = "WIFI_AUTH_OPEN";
      break;
      case WIFI_AUTH_WEP:
      authmode = "WIFI_AUTH_WEP";
      break;
      case WIFI_AUTH_WPA_PSK:
      authmode = "WIFI_AUTH_WPA_PSK";
      break;
      case WIFI_AUTH_WPA2_PSK:
      authmode = "WIFI_AUTH_WPA2_PSK";
      break;
      case WIFI_AUTH_WPA_WPA2_PSK:
      authmode = "WIFI_AUTH_WPA_WPA2_PSK";
      break;
      default:
      authmode = "Unknown";
      break;
    }
    printf("%26.26s    |    % 4d    |    %22.22s\n",list[i].ssid, list[i].rssi, authmode);
    if (i < 3)
    for (int k = 0 ; k < 6 ; k++)
    bssid_data[i][k] = list[i].bssid[k];
  }
  free(list);
  printf("\n\n");
}


void wifi_set_ssid(void)
{
#if !WITH_SMARTCONFIG

  int32_t base_stations_count = sizeof(base_stations) / sizeof(base_station_t);
  DBG_LOGD(TAG, "Number of base stations: %d", base_stations_count);
  if (base_stations_count == 1) {
    bs_pos = 0;
  } else {
    do {
      bs_pos = esp_random() % base_stations_count;
    } while (bs_pos == bs_pos_old);
  }
  bs_pos_old = bs_pos;

  DBG_LOGI(TAG, "Selected SSID %d", bs_pos);

  strcpy((char *)wifi_config.sta.ssid, base_stations[bs_pos].ssid);
  strcpy((char *)wifi_config.sta.password, base_stations[bs_pos].password);
#endif
}

char * wifi_get_ssid(void)
{
  return (char *)wifi_config.sta.ssid;
}

void wifi_save_ssid(void)
{
  DBG_LOGI(TAG, "Saving SSID");
  arch_nvs_write("ssid", wifi_config.sta.ssid, 32);
  arch_nvs_write("pwd", wifi_config.sta.password, 64);
}

void wifi_load_ssid(void)
{
  DBG_LOGI(TAG, "Loading SSID")
  if (arch_nvs_read("ssid", wifi_config.sta.ssid, 32) == 0 || arch_nvs_read("pwd", wifi_config.sta.password, 64) == 0) {
    DBG_LOGW(TAG, "No SSID stored");
    _wifi_has_ssid = 0;
    //wifi_config.sta.ssid[0] = 0;
  }
}

void wifi_reset_ssid(void)
{
  memset(wifi_config.sta.ssid, 0, 32);
  memset(wifi_config.sta.password, 0, 64);
  wifi_save_ssid();
}

static void wifi_sc_callback(smartconfig_status_t status, void *pdata)
{
  switch (status) {
    case SC_STATUS_WAIT:
        DBG_LOGI(TAG, "SC_STATUS_WAIT");
        break;
    case SC_STATUS_FIND_CHANNEL:
        DBG_LOGI(TAG, "SC_STATUS_FINDING_CHANNEL");
        event_set(EVENT_GROUP_WIFI, WIFI_EVENT_SC_STARTED);
        break;
    case SC_STATUS_GETTING_SSID_PSWD:
        DBG_LOGI(TAG, "SC_STATUS_GETTING_SSID_PSWD");
        break;
    case SC_STATUS_LINK:
        DBG_LOGI(TAG, "SC_STATUS_LINK");
        wifi_config_t *config = pdata;
        memcpy(wifi_config.sta.ssid, config->sta.ssid, 32);
        memcpy(wifi_config.sta.password, config->sta.password, 64);
        wifi_save_ssid();
        //wifi_connect();
        break;
    case SC_STATUS_LINK_OVER:
        DBG_LOGI(TAG, "SC_STATUS_LINK_OVER");
        if (pdata != NULL) {
            uint8_t phone_ip[4] = { 0 };
            memcpy(phone_ip, (uint8_t* )pdata, 4);
            DBG_LOGI(TAG, "Phone ip: %d.%d.%d.%d\n", phone_ip[0], phone_ip[1], phone_ip[2], phone_ip[3]);
        }
        event_set(EVENT_GROUP_WIFI, WIFI_EVENT_SC_DONE);
        break;
    default:
        DBG_LOGI(TAG, "Unhandled SmartConfig status: 0x%08x", status);
        break;
  }
}


static esp_err_t event_handler(void *ctx, system_event_t *event)
{

  switch(event->event_id) {

    case SYSTEM_EVENT_SCAN_DONE:
      wifi_scan_event(event);
      wifi_connect();
      break;

    case SYSTEM_EVENT_STA_START:
      wifi_connect();
      break;

    case SYSTEM_EVENT_STA_CONNECTED:
      event_set(EVENT_GROUP_WIFI, WIFI_EVENT_CONNECTED);
      watchdog = 0;
      watchdog_enabled = true;
      break;

    case SYSTEM_EVENT_STA_GOT_IP:
      DBG_LOGI(TAG, "Got IP");
      event_set(EVENT_GROUP_WIFI, WIFI_EVENT_GOT_IP);
      wifi_watchdog = 0;
      #if WITH_SIGNAL_LED
      for(int i = 0 ; i < 5 ; i++)
      {
        gpio_set_level(GPIO_NUM_18, 1);
        vTaskDelay(200 / portTICK_RATE_MS);
        gpio_set_level(GPIO_NUM_18, 0);
        vTaskDelay(200 / portTICK_RATE_MS);
      }
      #endif
      break;

    case SYSTEM_EVENT_STA_LOST_IP:
      DBG_LOGI(TAG, "### IP Lost");
    case SYSTEM_EVENT_STA_DISCONNECTED:
      /* This is a workaround as ESP32 WiFi libs don't currently
      auto-reassociate. */
      wifi_reconnect();
      break;

    default:
      break;
  }
  return ESP_OK;
}


void wifi_connect(void)
{
  wifi_set_ssid();
  esp_wifi_disconnect();

  DBG_LOGI(TAG, "SSID:     %s", wifi_config.sta.ssid);
  DBG_LOGI(TAG, "PASSWORD: %s", wifi_config.sta.password);

  if (_wifi_has_ssid == 0 || wifi_config.sta.ssid[0] == 0) {
    DBG_LOGI(TAG, "Connection Timeout, Starting SmartConfig");
    wifi_sc_start_task();
    _wifi_has_ssid = 5;
    return;
  } else {
    _wifi_has_ssid--;
  }

  esp_smartconfig_stop();

  DBG_LOGI(TAG, "Connecting to: %s", (char *)wifi_config.sta.ssid);
  ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
  esp_wifi_connect();
}


void wifi_reconnect(void)
{
  int w_delay;

  wifi_watchdog++;

  if (wifi_watchdog > 10) {
    DBG_LOGW(TAG, "WATCHDOG: Restarting WiFi");
    esp_wifi_stop();
    esp_wifi_start();
    wifi_watchdog = 0;
  }

  DBG_LOGI(TAG, "### Disconnected");

  event_clr(EVENT_GROUP_WIFI, 0xff);
  w_delay = ((esp_random() % 3) * 1000 ) / portTICK_RATE_MS;
  vTaskDelay(w_delay);
  wifi_connect();
}


void wifi_sc_task(void * parm)
{
    //EventBits_t uxBits;
    //ESP_ERROR_CHECK( esp_smartconfig_set_type(SC_TYPE_ESPTOUCH) );
    esp_smartconfig_set_type(SC_TYPE_ESPTOUCH);
    ESP_ERROR_CHECK( esp_smartconfig_start(wifi_sc_callback) );
    while (1) {
#if 0
        uxBits = xEventGroupWaitBits(s_wifi_event_group, CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY); 
        if(uxBits & CONNECTED_BIT) {
            DBG_LOGI(TAG, "WiFi Connected to ap");
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
            DBG_LOGI(TAG, "smartconfig over");
            esp_smartconfig_stop();
            vTaskDelete(NULL);
        }
#endif
      event_wait(EVENT_GROUP_WIFI, WIFI_EVENT_SC_ABORT | WIFI_EVENT_SC_DONE);
      if (event_get(EVENT_GROUP_WIFI, WIFI_EVENT_SC_ABORT)) {
        DBG_LOGI(TAG, "Aborting SmartConfig");
        wifi_load_ssid();
      }
      event_clr(EVENT_GROUP_WIFI, WIFI_EVENT_SC_STARTED | WIFI_EVENT_SC_ABORT | WIFI_EVENT_SC_DONE);
      esp_smartconfig_stop();
      event_set(EVENT_GROUP_WIFI, WIFI_EVENT_SC_COMPLETED);
      DBG_LOGI(TAG, "SmartConfig Completed");
      vTaskDelete(NULL);
    }
}

void wifi_sc_start(void)
{
  DBG_LOGI(TAG, "Starting SmartConfig");
  _wifi_has_ssid = 0;
  esp_wifi_disconnect();
}

void wifi_sc_start_task(void)
{
  DBG_LOGI(TAG, "Creating SmartConfig Task");
  xTaskCreate(wifi_sc_task, "wifi_sc_task", 4096, NULL, 3, NULL);
}

void wifi_init(void)
{
  tcpip_adapter_init();

  ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
  ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );

  wifi_load_ssid();

  wifi_set_ssid();
  DBG_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);

  ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
//  ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
  ESP_ERROR_CHECK( esp_wifi_start() );

  esp_wifi_get_mac(ESP_IF_WIFI_STA, sta_mac);


  DBG_LOGI(TAG,"| DeviceId =  %02X:%02X:%02X:%02X:%02X:%02X   |", sta_mac[0], sta_mac[1], sta_mac[2], sta_mac[3], sta_mac[4], sta_mac[5]);
}

void wifi_scan(void)
{
  wifi_scan_config_t scanConf = {
    .ssid = NULL,
    .bssid = NULL,
    .channel = 0,
    .show_hidden = true
  };

  ESP_ERROR_CHECK(esp_wifi_scan_start(&scanConf, false));
}
