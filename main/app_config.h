#ifndef _APP_CONFIG_H
#define _APP_CONFIG_H

#define WITH_SIGNAL_LED     (0)

#define WIFI_CONNECT_RETRIES            (5)
#define WIFI_SMARTCONFIG_WAIT_TIME      (15)
#define WITH_LCD_DISPLAY                (1)

#endif // _APP_CONFIG_H
