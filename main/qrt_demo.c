#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "qrt_demo.h"

#include "logger.h"
#include "arch_generic.h"

#include "tga.h"

#include "../res/c64font.h"


static const char *TAG = "qrt_demo";

#define QPL_BG_COL 0x000008
#define QPL_FG_COL 0x0910ff


// C64 font color 123, 113, 213        0x7b71d5
// C64 background color 65, 48, 164    0x4130a4


uint32_t qrt_buf[768] = { 
  0xff0000, 0, 0, 0, 0, 0, 0x00ff00, 0x0,
  0x0000ff, 0, 0, 0, 0, 0, 0, 0xff00ff,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0xff0000, 0, 0, 0, 0, 0, 0x00ff00, 0x0,
  0x0000ff, 0, 0, 0, 0, 0, 0, 0xff00ff,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0xff0000, 0, 0, 0, 0, 0, 0x00ff00, 0x0,
  0x0000ff, 0, 0, 0, 0, 0, 0, 0xff00ff,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,

  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 0, 0, 0, 0, 0,
  0xffffff, 0, 0, 0xff00ff, 0, 0, 0, 0x00ff00,
};


static uint32_t _saved_scr_pos;
static uint32_t _saved_scr_fg_col;
static uint32_t _saved_scr_bg_col;
static char *_saved_scr_ptr;
static uint16_t _saved_stage;

static uint32_t _scr_bg_col;
static uint32_t _scr_fg_col;
static uint32_t _scr_pos = 0;
static char *_scr_ptr;
static uint16_t _stage = 0;





void qrt_scroll_set(char *scr, uint32_t bg_col, uint32_t fg_col) {
    _saved_scr_bg_col = _scr_bg_col;
    _saved_scr_fg_col = _scr_fg_col;
    _saved_scr_pos = _scr_pos;
    _saved_scr_ptr = _scr_ptr;

    _scr_ptr = scr;
    _scr_fg_col = fg_col;
    _scr_bg_col = bg_col;
    _scr_pos = 0;

    _saved_stage = _stage;
}

void qrt_scroll_revert(void) {
    _stage = _saved_stage;
    _scr_ptr = _saved_scr_ptr;
    _scr_pos = _saved_scr_pos;
    _scr_fg_col = _saved_scr_fg_col;
    _scr_bg_col= _saved_scr_bg_col;
}


static void _print_font(int8_t x, int8_t y, const char c, uint32_t col) {
  uint32_t pos = (c - 32) * 8;
  uint8_t b;
  
  int32_t i, j;

  uint32_t *bx;

  if ((x + 8) < 0) return;
  bx = qrt_buf + (x * 8);
  for (i = x ; i < (x + 8) && i < (QRT_BUF_SIZE / 8) ; i++) {
    if (i >= 0) {
      b = fnt_c64[pos];
      for ( j = y ; j < 8 ; j++) {
        if (b & 0x80) {
          if (j >= 0) bx[j] = col;
        }
        b <<= 1;
      }
    }
    pos++;
    bx += 8;
  }
}


static char _scr_tab[256*256+30] = "                 NOW IT'S TIME TO PLAY!!! YOU CAN PLAY WITH SCROLL!!!!               ";

static uint16_t _scr_inject_ptr = 0;
static uint16_t _scr_play_ptr = 0;

void qrt_scroll_init(void) {
    memset(_scr_tab + 256 * 256, 32, 30);
    _scr_tab[256*256+12] = 255;
}

static char _priority_msg[256];
static char _priority_msg_show = false;
const char *scroll = "           QUARTET! RULEZ!!! ~00ff00F STILL ALIVE IN 2019!!!! ~0910ffF WELL WELL, HERE WE ARE AT THE MOONSHINE DRAGONS PARTY 2019  THANK YOU FOR THE INVITATION. IT IS AN HONOUR FOR US TO BE HERE. WE COULD NOT ARRIVE WITH EMPTY HANDS SO WE HAVE MADE THIS TOTEM AS A GIFT FOR ORGANISER YUGORIN. QUARTET POZDRAWIA WSZYSTKICH LICZNIE ZGROMADZONYCH NA MOONSHINE DRAGONS PARTY 2019.  POZDRAWIAMY WSZYSTKICH NAWIEDZONYCH, TAK JAK MY, WIELBICIELI TECHNOLOGII CYFROWEJ I NIE TYLKO. SPECJALNE POZDROWIENIA OD QUARTETU DLA ARKA BRONOWICKIEGO (YUGORIN) ZA ZAPROSZENIE NAS NA NAJLEPSZE PARTY W GALAKTYCE. WITAMY AUTOMATEUSZA W QUARTECIE - DRUGIE POKOLENIE QUARTET V2.0                    ";
;

int8_t sin_tab[] = {
  0, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 59, 62, 65, 67, 70, 73, 75, 78, 80, 82, 85, 87, 89, 91, 94, 96, 98, 100, 102, 103, 105, 107, 108, 110, 112, 113, 114, 116, 117, 118, 119, 120, 121, 122, 123, 123, 124, 125, 125, 126, 126, 126, 126, 126, 127, 126, 126, 126, 126, 126, 125, 125, 124, 123, 123, 122, 121, 120, 119, 118, 117, 116, 114, 113, 112, 110, 108, 107, 105, 103, 102, 100, 98, 96, 94, 91, 89, 87, 85, 82, 80, 78, 75, 73, 70, 67, 65, 62, 59, 57, 54, 51, 48, 45, 42, 39, 36, 33, 30, 27, 24, 21, 18, 15, 12, 9, 6, 3, 0, -3, -6, -9, -12, -15, -18, -21, -24, -27, -30, -33, -36, -39, -42, -45, -48, -51, -54, -57, -59, -62, -65, -67, -70, -73, -75, -78, -80, -82, -85, -87, -89, -91, -94, -96, -98, -100, -102, -103, -105, -107, -108, -110, -112, -113, -114, -116, -117, -118, -119, -120, -121, -122, -123, -123, -124, -125, -125, -126, -126, -126, -126, -126, -127, -126, -126, -126, -126, -126, -125, -125, -124, -123, -123, -122, -121, -120, -119, -118, -117, -116, -114, -113, -112, -110, -108, -107, -105, -103, -102, -100, -98, -96, -94, -91, -89, -87, -85, -82, -80, -78, -75, -73, -70, -67, -65, -62, -59, -57, -54, -51, -48, -45, -42, -39, -36, -33, -30, -27, -24, -21, -18, -15, -12, -9, -6, -3
};

uint8_t _get_cx(uint32_t px) {
    uint8_t c;
    char *ptr = &_scr_tab[_scr_play_ptr];
    c = ptr[_scr_pos + px];
    if (c == 0 && px == 0) {
        // end of scroll
    }
    return c;
}

void _scr_adv_pos(void) {

}

int8_t qrt_inject_text(char *t) {
    int i;
    DBG_LOGI(TAG, "Injecting text at pos %d len %d", _scr_inject_ptr, strlen(t));
   while(*t) {
        _scr_tab[_scr_inject_ptr++] = *t++;
    }
    return 0;
}

void qrt_set_priority_msg(char *s) {
    int i;
    if (_priority_msg_show) {
        DBG_LOGW(TAG, "Already showing priority message");
        return;
    }
    _priority_msg_show = true;

    DBG_LOGI(TAG, "Setting priority message %s", s);

    memset(_priority_msg, 32, 256);
    for(i = 0 ; i < strlen(s) && s[i]; i++) {
        _priority_msg[i + 12] = s[i];        
    }
    _priority_msg[i + 24] = 0;
    qrt_scroll_set(_priority_msg, 0x000000, 0xffffff);
    _stage = 300;
}

uint32_t _get_val(char *p, uint32_t *v) {
    uint32_t val = 0;
    char c;
    uint32_t pos = 0;
    
    while(1) {
        c = *p++;
        if (c >= '0' && c <= '9') {
            c -= '0';
            val = (val << 4) | (c & 0xf);
        } else if (c >= 'a' && c <= 'f') {
            c -= 'f';
            val = (val << 4) | ((c & 0xf) + 10);
        } else {
            *v = val;
            return pos;
        }
        pos++;
    }
}

bool qrt_scroll(void) {

    static int bar_x = 0;
    static int sin_pos = 0;
    static int x = -1;
    static char c;
    static uint32_t i, j;
    uint32_t col;


    col = _scr_fg_col;

    bar_x = (sin_tab[sin_pos] + 127 ) / 4;
    sin_pos++;
    sin_pos &= 0xff;

    c = 0;
    for (i = 0 ; i < 8 ; i++) {
      for (j = 0 ; j < 8 ; j++) {
        qrt_buf[(bar_x + i) * 8 + j] = c << 8;
      }
      c += 4;
    }
    for (i = 0 ; i < 8 ; i++) {
      for (j = 0 ; j < 8 ; j++) {
        qrt_buf[(bar_x + 8 + i) * 8 + j] = c << 8;
      }
      c -= 4;
    }

    int pos_add = 1;
    uint32_t val;

    char *ptr = _scr_ptr + _scr_pos;
    uint32_t adder;
    char cx;

    for (i = 0 ; i < 13 ;)
    {
        c = *ptr++;
        adder = 1;
        switch (c) {
          
        case 0:
            _scr_pos = 0;
            return true;
            break;
        
        case 255:
            _scr_pos = 0;
            return false;

        case '~':
            adder = _get_val(ptr, &val);
            //DBG_LOGI(TAG, "Adder: %d val: %08x", adder, val);
            ptr += adder;
            cx = *ptr++;
            //DBG_LOGI(TAG, "CX %c", cx);
            adder++;

            switch(cx) {
            case 'F':
                col = val;
                DBG_LOGI(TAG, "Val %08x F", val);
                if (i == 0) { 
                    _scr_fg_col = col;
                    DBG_LOGI(TAG, "Set FG col %08x", _scr_fg_col);
                }
                break;
            case 'B':
                if (i == 0) { 
                    _scr_bg_col = col;
                    DBG_LOGI(TAG, "Set BG col %08x", _scr_bg_col);
                }
                break;
            default: 
                break;
            }

            if (i == 0) {
                pos_add += adder;
            }
            break;
        default:
            //if (x == 0) printf("%c", c);
            _print_font(x + i * 8, 1, c, col);
            i++;
            break;
        
        }


    }
    //if (x == 0) printf("\n");
    
    x--;
    if (x <= -8) {
      x = 0;
      //DBG_LOGI(TAG, "Adding pos to ptr %d %d ", _scr_pos, pos_add);
      _scr_pos += pos_add;

      if (_scr_pos > (strlen(scroll) - 12)) {
        ///DBG_LOGI(TAG, "Reset pos");
        _scr_pos = 0;
      }
    }

    return false;
}

static void _fill_buf(uint32_t c) {
    uint32_t i;

    for(i = 0 ; i < QRT_BUF_SIZE ; i++) {
      qrt_buf[i] = c;
    }
}

static uint32_t _fade_col;
static uint32_t _fade_step;
static uint32_t _fade_cur;
static uint32_t _fade_cyc;

static void _fade_bg_init(uint32_t start_col, uint32_t end_col, uint32_t steps) {
    int32_t csx;
    uint32_t isc = start_col;
    int32_t  cex;
    uint32_t iec = end_col;

    uint32_t oc = 0x000000;


    uint8_t i;
    for (i = 0 ; i < 3 ; i++) {
        csx = isc & 0x0000ff;
        isc >>= 8;
        cex = iec & 0x0000ff;
        iec >>= 8;

        cex -= csx;
        cex /= steps;

        oc >>= 8;
        oc = oc | ((cex & 0x0000ff) << 16);
    }
    _fade_col = start_col;
    _fade_step = oc;
    _fade_cyc = steps;
    _fade_cur = 0;

    DBG_LOGI(TAG, "FC: %08x FS: %08x ST: %08x", start_col, oc, steps);
}

static bool _fade_bg(void) {
    uint32_t cx;
    uint32_t ic = _fade_col;
    uint32_t oc = 0x000000;
    uint32_t fxx;
    uint32_t ifx = _fade_step;
    uint8_t i;
    for (i = 0 ; i < 3 ; i++) {
        cx = ic & 0x0000ff;
        ic >>= 8;

        fxx = ifx & 0x0000ff;
        ifx >>= 8;

        cx += (fxx * _fade_cur);

        oc >>= 8;
        oc = oc | ((cx & 0x0000ff) << 16);
    }
    _fill_buf(oc);
    DBG_LOGI(TAG, "OC: %08x", oc);
    _fade_cur++;
    if (_fade_cyc == _fade_cur)
        return true;
    return false;
}

const static uint16_t _cur_blink_tmr_reset = 10;
static uint16_t _cur_blink_tmr = 20;
static uint8_t  _cur_state = 1;
static uint8_t  _cur_pos = 0;

static void _cursor_draw(void)  {
    if (_cur_state == 0) return;
    if (_cur_pos > 11) return;
    uint16_t i;
    for(i = 0 ; i < 8 * 8 ; i++) {
        qrt_buf[_cur_pos * 64 + i] = _scr_fg_col;
    }
}


static const char *_str_load = "LOAD \"*\",8,1";

static bool qrt_print_load(void) {
    static uint8_t str_pos = 0;
    static uint8_t wait_tmr = 100;
    
    _fill_buf(QPL_BG_COL);

    if (_cur_blink_tmr-- == 0) {
        _cur_blink_tmr = _cur_blink_tmr_reset;
        _cur_state ^= 1;
    }
    _cursor_draw();
    uint8_t i;
    if (wait_tmr-- == 0) {
        wait_tmr = 50;
        if (str_pos < 12) {
            str_pos++;
            _cur_pos++;
        } else {
            return true;
        }
    }
    for(i = 0 ; i < str_pos ; i++) {
        _print_font(i * 8, 1, _str_load[i], QPL_FG_COL);
    }
    return false;
}

static bool qrt_strobo(void) {
    static uint8_t timer = 5;
    static uint8_t next_timer = 3;
    static uint8_t repeat = 5;
    static uint32_t col = 0x000000;
    if (timer-- == 0) {
        timer = next_timer;
        col ^= 0xffffff;
        if (repeat-- == 0) {
            repeat = 20;
            next_timer--;
        }
    }
    _fill_buf(col);
    if (next_timer == 0) return true;
    return false;
}

static bool qrt_strobo_short(void) {
    static uint8_t timer = 5;
    static uint8_t next_timer = 2;
    static uint8_t repeat = 5;
    static uint32_t col = 0xff0000;
    if (timer-- == 0) {
        timer = next_timer;
        col ^= 0xffffff;
        if (repeat-- == 0) {
            repeat = 20;
            next_timer--;
        }
    }
    _fill_buf(col);
    if (next_timer == 0) {
        timer = 5;
        next_timer = 2;
        repeat = 5;
        return true;
    }
    return false;
}


#define pi M_PI

void plasmaScreen(int width,int height){
 
	int x,y;
    static int sec = 0;
	float dx,dy,dv;
	//time_t t;
 
	//while(1){
		//time(&t);
		//sec = (localtime(&t))->tm_sec;
        sec++;
	for(x = 0;x<width;x++) {
		for(y=0;y<height;y++){
			dx = x + .5 * sin(sec/5.0);
			dy = y + .5 * cos(sec/3.0);
 
			dv = sin(x*10 + sec) + sin(10*(x*sin(sec/2.0) + y*cos(sec/3.0)) + sec) + sin(sqrt(100*(dx*dx + dy*dy)+1) + sec);
 
			uint32_t r = 255 * fabs(sin(dv * pi));
            uint32_t g = 255 * fabs(sin(dv * pi + 2 * pi / 3));
            uint32_t b = 255 * fabs(sin(dv * pi + 4 * pi / 3));

            uint32_t c  = ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);

            qrt_buf[y *  8 + x] = c;
		}
	}
}

static const char *_qrt_str_quartet = "QUARTET 2019";

static bool qrt_plasma(void) {
    uint8_t i;
    plasmaScreen(8, 96);
    for(i = 0 ; i < 12 ; i++) {
        _print_font(i * 8, 1, _qrt_str_quartet[i], 0x000);
    }
    return false;
}


void _render_init(uint8_t *tga) {

}

#include "../res/star.h"

void _render_gfx(const uint8_t *s, int32_t x, int32_t y, uint32_t c) {
    uint8_t w = *s++;
    uint8_t h = *s++;

    int32_t i, j;
    int32_t m;
    if (x >= 8) return;
    if ((x + w) < 0) return;



    for (j = 0 ; j < h ; j++) {
        if (x > 0) m = x, i = 0;
        else m = 0, i = 0 - x;
        for ( ; m < 8 && i < w ; i++, m++) {
            if (s[j * w + i]) qrt_buf[j * 8 + m] = c; 
        }
    }
}

void _render_gfx_with_pal(const uint8_t *s, int32_t x, int32_t y, const uint8_t *pal) {
    uint8_t w = *s++;
    uint8_t h = *s++;

    int32_t i, j;
    int32_t m;
    if (x >= 8) return;
    if ((x + w) < 0) return;

    uint32_t c;
    uint32_t px;

    for (j = 0 ; j < h ; j++) {
        if (x > 0) m = x, i = 0;
        else m = 0, i = 0 - x;
        for ( ; m < 8 && i < w ; i++, m++) {
            px = s[j * w + i];
            px *= 3;
            if (px) {
                c = (pal[px + 1] / 4) << 16 | (pal[px + 0] / 4) << 8 | (pal[px +2] / 4);
                //printf("%08x\n", c);
                qrt_buf[j * 8 + m] = c; 
            }
        }
    }
}

static const char *_qrt_str_qrt = "QRT!";

static bool qrt_commie(void) {
    static int32_t pos = 0;
    static uint32_t time = 0;

    
    time ++;
    
    uint32_t c;
    if (time < 256)
        c = 0x00ff00;
    if (time >= 256 && time < 511) {
        time += 4;
        uint32_t b = time & 0xff;
        uint32_t r = (255 - time) & 0xff;
        c = (r << 8) | b;
    }
    if (time > 510)
        c = 0x0000ff;

    if (time > 1000) return  true;

    _fill_buf(c);
    pos += 1;
    if (pos > 256) pos = 0;

    int32_t x = sin((2 * M_PI * pos) / 256) * 36 - 28;
    DBG_LOGI(TAG, "Pos %d", x);
    _render_gfx(_gfx_star, x, 0, 0x204000);

    uint32_t i;
    for(i = 0 ; i < 4 ; i++) {
        _print_font((i + 8) * 8, 1, _qrt_str_qrt[i], 0xffffff);
    }

    return false;
}

#include "../res/qrt.h"

__attribute__((unused)) static bool qrt_logo_wave(void) {
    static int32_t pos = 0;
    _fill_buf(0x000000);
    pos += 1;
    if (pos > 256) pos = 0;

    int32_t x = sin((2 * M_PI * pos) / 256) * (158 / 2) - (158 / 2) + 8;
    _render_gfx_with_pal(_gfx_qrt, x, 0, _pal_qrt);
    return false;
}


void qrt_loop(void) {
    static uint32_t timer = 0;

    switch(_stage) {
        case 0:
            timer = 0;
            // qrt_plasma();
            // qrt_strobo();
            // qrt_commie();
            _fade_bg_init(0x000000, QPL_BG_COL, 0x08);
            _scr_fg_col = QPL_FG_COL;
            _scr_bg_col = QPL_BG_COL;
            //qrt_logo_wave();
            // _fade_bg_init(0x000000, 0x00ff00, 0x20);
            _stage++;
            break;
        case 1:
            if (_fade_bg())
                _stage++;
            sleep_ms(200);
            break;
        case 2:
            if (qrt_print_load()) {
                _stage++;
                sleep_ms(1000);
            }
            break;
        case 3:
            if (qrt_strobo()) {
                _stage++;
                timer = 0;
                sleep_ms(1000);
            }
            break;
        case 4:
            qrt_plasma();
            if (timer >= 200) {
                _stage++;
                timer = 0;
            }
            timer++;
            break;
        case 5:
            if (qrt_commie()) {
                _stage++;
            }
            break;

        // MAIN SCROLL
        case 6:
            qrt_scroll_set(scroll, QPL_BG_COL, QPL_FG_COL);
            _stage++;
            break;

        case 7:
            _fill_buf(_scr_bg_col);
            if (qrt_scroll()) {
                _stage++;
            }
            break;

        // PARTY CURATED SCROLL
        case 8:
            qrt_scroll_set(_scr_tab, QPL_BG_COL, QPL_FG_COL);
            _stage++;
            break;
        case 9:
            _fill_buf(_scr_bg_col);
            if (qrt_scroll()) {
                _scr_pos = 0;
                //_stage = 5;
            }
            break;
        
        // PRIORITY MESSAGE
        case 300:
            if (qrt_strobo_short()) {
                _stage++;
            }
            break;
        case 301:
            _fill_buf(_scr_bg_col);
            if (qrt_scroll()) {
                qrt_scroll_revert();
                _priority_msg_show = false;
            }
            break;

        default:
            break;
    }
}