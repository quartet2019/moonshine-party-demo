#include "freertos/FreeRTOS.h"
#include "esp_system.h"

#include "board_control.h"
#include "logger.h"

const static char *TAG = "GPIOCX";


#if 0
void gpio_config_button(void)
{
  gpio_config_t btn_config;
  btn_config.intr_type = GPIO_INTR_ANYEDGE;    //Enable interrupt on both rising and falling edges
  btn_config.mode = GPIO_MODE_INPUT;           //Set as Input
  //btn_config.pin_bit_mask = (1 << BUTTON_GPIO); //Bitmask
  btn_config.pull_up_en = GPIO_PULLUP_DISABLE;    //Disable pullup
  btn_config.pull_down_en = GPIO_PULLDOWN_ENABLE; //Enable pulldown
  gpio_config(&btn_config);
  printf("Button configured\n");

  //Configure interrupt and add handler
  //gpio_install_isr_service(0);                  //Start Interrupt Service Routine service
  //gpio_isr_handler_add(BUTTON_GPIO, isr_button_pressed, NULL); //Add handler of interrupt
  printf("Interrupt configured\n");

}

#endif

int32_t _bx_btn_state = 0;

int32_t board_get_button(void)
{
    if (_bx_btn_state % 2 == 0 && _bx_btn_state != 0) {
      _bx_btn_state = 0;
      return 1;
    }
    return 0;
}


void bx_button_pressed(void *args)
{
  _bx_btn_state++;// = gpio_get_level(BX_BUTTON);
  //DBG_LOGI(TAG, "Button Pressed %d", btn_state);
  //gpio_set_level(LED_GPIO,btn_state);
}


void board_init(void)
{
  DBG_LOGI(TAG, "Configuring Board");
  //Configure button
  gpio_config_t btn_config;
  btn_config.intr_type = GPIO_INTR_ANYEDGE;    //Enable interrupt on both rising and falling edges
//  btn_config.intr_type = GPIO_PIN_INTR_DISABLE;
  btn_config.mode = GPIO_MODE_INPUT;           //Set as Input
  btn_config.pin_bit_mask = (1 << BX_BUTTON); //Bitmask
  btn_config.pull_up_en = GPIO_PULLUP_DISABLE;    //Disable pullup
  btn_config.pull_down_en = GPIO_PULLDOWN_ENABLE; //Enable pulldown
  gpio_config(&btn_config);
  printf("Button configured\n");

  //Configure LED
  gpio_pad_select_gpio(BX_LED);               //Set pin as GPIO
  gpio_set_direction(BX_LED, GPIO_MODE_OUTPUT);   //Set as Output
  printf("LED configured\n");

  //Configure interrupt and add handler
  gpio_install_isr_service(0);                  //Start Interrupt Service Routine service
  gpio_isr_handler_add(BX_BUTTON, bx_button_pressed, NULL); //Add handler of interrupt
  // DBG_LOGI(TAG, "Interrupt configured\n");
  DBG_LOGI(TAG, "Board Configured");
}