#ifndef _LOGGER_H
#define _LOGGER_H

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if __GNUC__ > 3
#include <stdint.h>
#include <inttypes.h>
#include <sys/time.h>
#elif defined(CONFIG_PLATFORM_8711B)
#include <stdint.h>
#include <inttypes.h>
#include <local_time.h>
#endif

#define WITH_LOGGLY         (0)

int32_t log_timestamp(void);


#define LOG_COLOR_BLACK   "30"
#define LOG_COLOR_RED     "31"
#define LOG_COLOR_GREEN   "32"
#define LOG_COLOR_BROWN   "33"
#define LOG_COLOR_BLUE    "34"
#define LOG_COLOR_PURPLE  "35"
#define LOG_COLOR_CYAN    "36"
#define LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
#define LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
#define LOG_RESET_COLOR   "\033[0m"
#define LOG_COLOR_E       LOG_COLOR(LOG_COLOR_RED)
#define LOG_COLOR_W       LOG_COLOR(LOG_COLOR_BROWN)
#define LOG_COLOR_I       LOG_COLOR(LOG_COLOR_GREEN)
#define LOG_COLOR_D
#define LOG_COLOR_V

#ifndef PRId32
#define PRId32 "ld" 
#endif

#ifndef LOG_FORMAT
#define LOG_FORMAT(letter, format)  LOG_COLOR_ ## letter #letter " (%" PRId32 ") %s: " format LOG_RESET_COLOR "\n"
#endif

#if 0
#define ESP_LOG_LEVEL(level, tag, format, ...) do {                     \
        if (level==ESP_LOG_ERROR )          { esp_log_write(ESP_LOG_ERROR,      tag, LOG_FORMAT(E, format), esp_log_timestamp(), tag, ##__VA_ARGS__); } \
        else if (level==ESP_LOG_WARN )      { esp_log_write(ESP_LOG_WARN,       tag, LOG_FORMAT(W, format), esp_log_timestamp(), tag, ##__VA_ARGS__); } \
        else if (level==ESP_LOG_DEBUG )     { esp_log_write(ESP_LOG_DEBUG,      tag, LOG_FORMAT(D, format), esp_log_timestamp(), tag, ##__VA_ARGS__); } \
        else if (level==ESP_LOG_VERBOSE )   { esp_log_write(ESP_LOG_VERBOSE,    tag, LOG_FORMAT(V, format), esp_log_timestamp(), tag, ##__VA_ARGS__); } \
        else                                { esp_log_write(ESP_LOG_INFO,       tag, LOG_FORMAT(I, format), esp_log_timestamp(), tag, ##__VA_ARGS__); } \
    } while(0)
#endif

#define DBG_LOGI(tag, format, ...) {printf(LOG_FORMAT(I, format), log_timestamp(), tag, ##__VA_ARGS__); }
#define DBG_LOGW(tag, format, ...) {printf(LOG_FORMAT(W, format), log_timestamp(), tag, ##__VA_ARGS__); }
#define DBG_LOGD(tag, format, ...) {printf(LOG_FORMAT(D, format), log_timestamp(), tag, ##__VA_ARGS__); }
#define DBG_LOGE(tag, format, ...) {printf(LOG_FORMAT(E, format), log_timestamp(), tag, ##__VA_ARGS__); }


#endif // _LOGGER_H
