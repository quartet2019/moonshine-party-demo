#ifndef _WIFI_H
#define _WIFI_H

void wifi_init(void);
const char * wifi_get_mac_string(uint8_t *mac);
char * wifi_get_ssid(void);
void wifi_disconnect(void);
void wifi_reconnect(void);
void wifi_connect(void);
void wifi_scan(void);
uint8_t * wifi_get_mac(void);
void wifi_sc_start(void);
void wifi_sc_start_task(void);
void wifi_reset_ssid(void);

#endif // _WIFI_H
