#include <stdlib.h>
#if !defined(CONFIG_PLATFORM_8711B)
#include <sys/time.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#if __GNUC__ > 3
#include <stdint.h>
#endif

#include "logger.h"

#if WITH_LOGGLY
#include "esp_request.h"
#endif

static char buffer[256];

#if WITH_LOGGLY
static int32_t len_to_send;
#endif

int32_t log_timestamp(void)
{
  struct timeval tv;
  static int last_sec = 0;
  int32_t time_elapsed;
  gettimeofday(&tv,NULL);
  if (last_sec == 0)
    last_sec = tv.tv_sec;

  time_elapsed = tv.tv_sec - last_sec;
  return time_elapsed;
  //return tv.tv_sec; // seconds
  //return tv.tv_usec; // microseconds
}

#if WITH_LOGGLY

static int log_upload_callback(request_t *req, char *data, int len)
{
  return len;
}

void log_send_to_loggly(const char *buf)
{
  request_t *req;

  const char *url = "http://logs-01.loggly.com/inputs/84825cee-bd9c-4acb-a521-b4fed186cf7d/tag/http/";

  req = req_new(url);
  req_setopt(req, REQ_SET_METHOD, "GET");

/*
  sprintf(data, "x-esp32-sta-mac:%02X:%02X:%02X:%02X:%02X:%02X", sta_mac[0], sta_mac[1], sta_mac[2], sta_mac[3], sta_mac[4], sta_mac[5]);
  req_setopt(req, REQ_SET_HEADER, data);

  sprintf(data, "x-esp32-version:%s", APP_VERSION);
  req_setopt(req, REQ_SET_HEADER, data);
*/

  req_setopt(req, REQ_FUNC_UPLOAD_CB, log_upload_callback);
  req_perform(req);
  req_clean(req);
}


#endif // WITH_LOGGLY



void log_printf(char * format, ...)
{
  va_list args;
  va_start (args, format);
  vsnprintf (buffer, 255, format, args);
  va_end (args);

  printf("%s", buffer);
#if WITH_LOGGLY

  len_to_send = strlen(buffer);

#endif // WITH_LOGGLY
}


