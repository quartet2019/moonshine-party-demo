#ifndef _ARCH_H
#define _ARCH_H

void sleep_ms(uint32_t ms);
void arch_nvs_init(void);
int arch_nvs_read(const char *name, void *data, size_t size);
int arch_nvs_write(const char *name, void *data, size_t size);
void arch_rng_init(void);
void arch_restart(void);
uint32_t arch_random(void);
uint32_t arch_geo(int s);
uint32_t arch_get_ms(void);

#endif // _ARCH_H
