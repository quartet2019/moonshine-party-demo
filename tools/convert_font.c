
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "../res/c64font_tga.h"


uint8_t *fnt_ptr = Fonts_C64_one_set_bitmap_tga + 0x2292;


char *font_chars[] = {
    "@abcdefghijklmno",
    "pqrstuvwxyz[~]~~",
    " !\"#$%&'()*+,-./",
    "0123456789:;<=>?",
    "~ABCDEFGHIJKLMNO",
    "PQRSTUVWXYZ~~~~~",
};

uint8_t fnt_new[256*8];


uint8_t fnt_max = 0;
uint8_t fnt_min = 255;

uint8_t make_byte(uint8_t *p) {
    
    uint8_t b = 0;

    for(int i = 0 ; i < 8 ; i++) {
        //printf("%02x", *p);
        if (*p++ == 0) {
            b |= 1;
        }
        b <<= 1;
    }
    //printf("\n");
    return b;
}


void make_font(uint8_t *p, uint8_t f) {
    for(int i = 0 ; i < 8 ; i++) {
        fnt_new[f * 8 + i] = make_byte(p);
        //printf("%08x %02x\n", (int)p, fnt_new[f * 8 + i]);
        p -= 8 * 16;
    }
}


void fill_table(uint8_t line, uint8_t *s) {
    uint8_t c;

    for(int i = 0 ; i < 16 ; i++) {
        c = *s++;
        if (c != '~') {
            if (c > fnt_max) fnt_max = c;
            if (c < fnt_min) fnt_min = c;

            int font_addr = (line * 8 * 8 * 16);
            //printf("FNT_ADDR: %d\n", font_addr);
            make_font(fnt_ptr - font_addr + i * 8, c);
        }
    }
}

int main(int argc, char **argv)
{
    printf("uint8_t fnt_c64[] = {\n");
    int i, j;
    for(i = 0 ; i < 8 * 256 ; i++) {
        fnt_new[i] = 0;
    }
    for(i = 0 ; i < 6 ; i++) {
        fill_table(i, (uint8_t *)font_chars[i]);
    }
    for(i = fnt_min ; i <= fnt_max ; i++) {
        printf("\t");
        for(j = 0 ; j < 8 ; j++) {
            printf("0x%02x, ", fnt_new[i*8 + j]);
        }
        if ((char)i != '\\') printf("// %c", (char)i);
        printf("\n");
    }
    printf("};\n");
    return 0;
}