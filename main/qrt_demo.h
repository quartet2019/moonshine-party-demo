#ifndef _QRT_DEMO_H
#define _QRT_DEMO_H

#include <stdint.h>


#define QRT_BUF_SIZE 768

extern uint32_t qrt_buf[];

void qrt_loop(void);
int8_t qrt_inject_text(char *t);
void qrt_set_priority_msg(char *s);

#endif // _QRT_DEMO_H
