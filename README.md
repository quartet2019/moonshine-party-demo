# Quartet Demo for Moonshine Dragons Party


## Checkout repo

git submodule init
git submodule update --recursive

## Build tools

cd tools
make
cd ..

## Build resources

cd res
make
cd ..

## Build app

make menuconfig

(change port for yorr serial flasher for ESP) EXIT and SAVE

make flash monitor -j 32

